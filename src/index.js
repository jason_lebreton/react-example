//  Import des libraires React & ReactDOM
import React from 'react';
import ReactDOM from 'react-dom';

//  Import de nos composants
import Comment from './components/CommentDetails';
import Approval from './components/ApprovalCard';
import Feed from './components/StateFeed';
import Item from './components/StateItem';

//  Creation des composants React
class App extends React.Component {
    commentsList = [
        {
            id: 0,
            author: "Tommy",
            content: "Un message comme un autre.",
            date: "Lundi à 6:04",
            approved: null
        },
        {
            id: 1,
            author: "Sam",
            content: "Un autre.",
            date: "Lundi à 6:04",
            approved: null
        },
        {
            id: 2,
            author: "Robert",
            content: "Paradoxal ?",
            date: "Lundi à 6:07",
            approved: null
        },
        {
            id: 3,
            author: "Janna",
            content: "De plus en plus étrange cet exemple...",
            date: "Lundi à 6:09",
            approved: null
        },
        {
            id: 4,
            author: "Poppy",
            content: "Je pense qu'on va s'arrêter là.",
            date: "Lundi à 6:15",
            approved: null
        }
    ];

    setApprovalState = action => {
        console.log("ACTION", action);
        setTimeout(() => { console.log("CHILDREN", this._reactInternalFiber.child) }, 10);

        this.commentsList[2].approved = true;
    }

    render() {
        return (
            <div className="ui container comments">
                <br />
                <div style={{float: "left"}}>
                    <h2 className="ui icon center aligned header">
                        <i className="settings icon"></i>
                        <div className="content">
                            Commentaires récents
                            <div className="sub header">Liste des commentaires à approuver</div>
                        </div>
                    </h2>
                    {this.commentsList.map(c => {
                        return(
                            <Approval changeApprovalState={this.setApprovalState.bind(c)} key={`approval[${c.id}]`}>
                                <Comment
                                    item={c}
                                />
                            </Approval>
                        );
                    })}
                </div>
                <div style={{float: "right"}}>
                    <h2 className="ui icon center aligned header">
                        <i className="settings icon"></i>
                        <div className="content">
                            Feed des approbations
                            <div className="sub header">Feed des modifications d'approbations</div>
                        </div>
                    </h2>
                    {this.commentsList.map(c => {
                        return(
                            <Feed key={`feed[${c.id}]`}>
                                <Item
                                    item={c}
                                />
                            </Feed>
                        );
                    })}
                </div>
            </div>
        );
    }
}

//  Rendu des composants
ReactDOM.render(
    <App />,
    document.querySelector("#root")
);
