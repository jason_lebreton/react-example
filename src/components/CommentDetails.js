import React from 'react';

class CommentDetails extends React.Component {
    constructor(props) {
        super(props);

        this.props = props;
        this.state = {};
    }

    render() {
        return(
            <div className="comment">
                <a href="/" className="avatar">
                    <i className="settings icon"></i>
                </a>
                <div className="content">
                    <a href="/" className="author">
                        {this.props.item.author}
                    </a>
                    <div className="metadata">
                        <span className="date">{this.props.item.date}</span>
                    </div>
                    <div className="text">{this.props.item.content}</div>
                </div>
            </div>
        );
    }
}

export default CommentDetails;
