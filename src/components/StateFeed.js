import React from 'react';


class StateFeed extends React.Component {
    constructor(props) {
        super(props);

        this.state = {};
        this.props = props;
    }

    render() {
        return (
            <div className="ui items">
                {this.props.children}
            </div>
        );
    }
}
export default StateFeed;
