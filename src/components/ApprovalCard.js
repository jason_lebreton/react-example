import React from 'react';


class ApprovalCard extends React.Component {
    state = {};

    constructor(props) {
        super(props);

        this.props = props;
        this.state = this.props.item;
    }

    onApprovalAction = event => {
        event.preventDefault();
        let props = this.props.children.props;

        props.item.approved = event.target.className.indexOf("green") > -1;
        this.setState(props.item);

        // setTimeout(() => {  }, 10);
        this.props.changeApprovalState(this);
    }

    render() {
        return (
            <div className="ui card">
                <div className="content">{this.props.children}</div>
                <div className="extra content">
                    <div className="ui two buttons">
                        <button className="ui basic green button" onClick={this.onApprovalAction.bind(this)}>Approuver</button>
                        <button className="ui basic red button" onClick={this.onApprovalAction.bind(this)}>Rejetter</button>
                    </div>
                </div>
            </div>
        );
    }
}
export default ApprovalCard;
