import React from 'react';


class StateItem extends React.Component {
    constructor(props) {
        super(props);

        this.props = props;
        this.state = { item: this.props.item };
    }

    render() {
        return (
            <div className="item">
                <div className="content">
                    <a href="/" className="header">
                        <i className="settings icon"></i> Commentaire
                    </a>
                    <div className="meta">
                        <span>{this.state.item.author}</span>
                    </div>
                    <div className="extra">
                        {this.state.item.approved == null ? "À traiter" : (this.state.item.approved ? "Approuvé" : "Non approuvé") }
                    </div>
                </div>
            </div>
        );
    }
}
export default StateItem;
